# *-* coding: utf-8 *-*
from flask import Flask, request, url_for, redirect, Blueprint
from flask import render_template
from forms import EqualeForm
import math
from api import VkontakteApi

app = Flask(__name__)
app.secret_key = 's3cr3t'
CSRF_ENABLED = True
vk = VkontakteApi()

site_index = Blueprint('equale', __name__, template_folder='templates')

@app.route('/equale', methods = ['GET', 'POST'])
def equale():
	form = EqualeForm()
	if form.validate_on_submit():
		city = form.city_field.data
		country = form.country_field.data
		tyan = vk.get_users(city, country)
		#return redirect('/equale' + '?city_field=' + a + '?country_field=' + b)
		return render_template('result.html', form=form, items=tyan)
	return render_template('index.html', form=form)
