# *-* coding: utf-8 *-*
from flask.ext.wtf import Form
from wtforms.fields import IntegerField, SelectField, TextField
from wtforms.validators import NumberRange
from wtforms import validators

class EqualeForm(Form):
    city_field = TextField('City', validators = [validators.required()])
    country_field = SelectField("Select", choices=[('RU', "Russia"), ('UA', "Ukraine"), ('BY', "Belarus"), ('KZ', "Kazahstan"), ('AM', "Armenya"), ('AF', "Afganistan")], default=('RU', "Россия"))
